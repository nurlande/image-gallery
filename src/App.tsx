import React, { useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
import { useAppDispatch, useAppSelector } from './redux/hooks';
import { fetchingImages, getImages } from './redux/imageActions';
import { RootState } from './redux/store';
import Images from './images/Images';

function App() {

  const dispatch = useAppDispatch()
  const loading = useAppSelector((state: RootState) => state.image.loading)

  useEffect(() => {
    dispatch(fetchingImages()) // set fetching state redux
    fetch('https://jsonplaceholder.typicode.com/photos').then(res => 
      res.json().then(d => {
        dispatch(getImages(d));
      })
    ) // set default list of images
  }, [dispatch])
  
  return (
    loading ? <div>Loading...</div> :
    <div className="myApp">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Images/>}/>
          <Route path="/:query" element={<Images/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;