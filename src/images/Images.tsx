import React, { useEffect, useState } from 'react'
import { Button, Col, Container, FormLabel, FormSelect, Pagination, Row } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { deleteImage, setImages } from '../redux/imageActions';
import { RootState } from '../redux/store';
import DeleteConfirm from './DeleteConfirm';
import FullView from './FullView'
import { Image, QueryParams } from './ImageModel'

export default function Images() {

    const ims = useAppSelector((state: RootState) => state.image.images); // list of images by page grouped 
    const albums = useAppSelector((state: RootState) => state.image.albums); // album list

    const dispatch = useAppDispatch()
    
    const params = useParams()
    const navigate = useNavigate()

    const [show, setShow] = useState(false)
    const [showDelete, setShowDelete] = useState(false)
    
    const [error, setError] = useState<string | null>(null)
    
    const [selectedImage, setSelectedImage] = useState<Image | null>(null)
    const [query, setQuery] = useState<QueryParams>({
        page: 1,
        sort: "none",
        filterAid: "All"
    })

    useEffect(() => {
        if(params.query) { 
            let q: string = params.query || ""
            try {
                let qp = JSON.parse('{"' + decodeURI(q?.replace(/&/g, "\",\"").replace(/=/g,"\":\"")) + '"}')
                // string of query params converting into object
                setQuery(qp)
                dispatch(setImages(qp)) // calling redux function to restructure image list due to query parameters
                setError(null)
            } catch {
                setError("URL is incorrect") // catch query params parse error
            }   
        } else {
            setError(null)
        }
    }, [params, dispatch])

    //delete confirm function
    const confirmDelete = () => {
        // we can also send delete request to restapi if exists
        dispatch(deleteImage(selectedImage?.id, query.page))
        setShowDelete(false)
    }

    // page, sort, filter function
    const toPage = (page: number) => {
        let path = "page=" + page + "&sort=" + query.sort + "&filterAid=" + query.filterAid
        navigate("/" + path)
    }

    const sort = (sortType: string) => {
        let path = "page=" + query.page + "&sort=" + sortType + "&filterAid=" + query.filterAid
        navigate("/" + path)
    }

    const filter = (alId: number | string) => {
        let path = "page=" + query.page + "&sort=" + query.sort + "&filterAid=" + alId
        navigate("/" + path)
    }

    return (
        error ? 
        <Container className="mt-4">
            {/* error indicatior */}
            <h5>{error}</h5> 
            <Button variant="primary" className="ml-2" onClick={() => navigate("/")}>Go Home</Button>
        </Container> :
        <div>
            <Container>
                <Row className='my-3'>
                    <Col sm="3">
                        <FormLabel>
                            Sort By Album: 
                        </FormLabel>
                        <FormSelect value={query.sort}
                                onChange={(e) => sort(e.target.value)}>
                            <option value="none">None</option>
                            <option value="asc">Ascending</option>
                            <option value="desc">Descending</option>
                        </FormSelect>
                        {/* sort by album id */}
                    </Col>
                    <Col sm="3">
                        <FormLabel>
                            Filter By Album:
                        </FormLabel>
                        <FormSelect value={query.filterAid} 
                                onChange={(e) => filter(e.target.value)}>
                            <option value="All">All</option>
                            {albums.map((alb: number, i: number) => 
                                <option key={i} value={alb}>{alb}</option>
                            )}
                        </FormSelect>
                        {/* filter by album select */}
                    </Col>
                </Row>
                {/* images view grid */}
                <Row xs={2} md={4} lg={6}>
                    {ims.find((group: any) => group.page === Number(query.page || 1))?.data
                        .map((img: Image, i: number) => 
                        <Col key={i}>
                            <img src={img.thumbnailUrl} alt={img.thumbnailUrl} style={{width: "100%", cursor: "pointer"}} 
                                onClick={() => {
                                    setSelectedImage(img)
                                    setShow(true)
                                    // full view function
                                }}/>
                            <Button variant="outline-danger" size="sm" className='my-1 flaot-left' onClick={() => {
                                setSelectedImage(img);
                                setShowDelete(true);
                                // delete function
                            }}>
                                Delete</Button>
                        </Col>
                    )}
                </Row>
                {/* Pagination by page */}
                <Pagination className='mt-3 pt-3 justify-content-end'>
                    {Number(query.page || 1) > 1 && <>
                        <Pagination.First onClick={() => toPage(1)}/>
                        <Pagination.Prev onClick={() => toPage(Number(query.page || 1) - 1)}/>
                        {/* <Pagination.Ellipsis /> */}
                    </>}
                    <Pagination.Item active>{query.page || 1}</Pagination.Item>
                    {ims.find((group: any) => group.page === (Number(query.page || 1) + 1)) && 
                        <>
                            {/* <Pagination.Ellipsis /> */}
                            <Pagination.Next onClick={() => toPage((Number(query.page || 0) + 1))}/>
                            <Pagination.Last onClick={() => toPage(Math.max(...ims.map((g: any)=>g.page)))}/>
                        </>}
                </Pagination>
            </Container>

            {/* Full view image on modal */}
            <FullView show={show} closeModal={() => setShow(false)} selectedImage={selectedImage}/>
            
            {/* Delete Confirmation modal */}
            <DeleteConfirm show={showDelete} closeModal={() => setShowDelete(false)} selectedImage={selectedImage} 
                confirmDelete={confirmDelete}/>         
        </div>
    )
}