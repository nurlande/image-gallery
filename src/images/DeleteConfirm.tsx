import React from 'react'
import { Button, Modal } from 'react-bootstrap';

interface DeleteConfirmProps {
    show: boolean;
    closeModal: any;
    selectedImage: any;
    confirmDelete: any;
}

export default function DeleteConfirm(props: DeleteConfirmProps) {
  
    return (
      <>
        <Modal show={props?.show} onHide={props?.closeModal}>
                <Modal.Header closeButton>
                <Modal.Title>Delete confirmation</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure to delete this image?</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={props?.closeModal}>
                    Close
                </Button>
                <Button variant="primary" onClick={props.confirmDelete}>
                    Confirm
                </Button>
                </Modal.Footer>
        </Modal>
      </>
    )
}