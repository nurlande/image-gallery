import React from 'react'
import { Modal } from 'react-bootstrap';
import { Image } from './ImageModel';

interface FullViewProps {
    show: boolean;
    closeModal: any;
    selectedImage: Image | null;
}

export default function FullView(props: FullViewProps) {
  
    return (
      <>
        <Modal show={props?.show} onHide={props?.closeModal}>
            <img alt="full view" src={props?.selectedImage?.url}/>
        </Modal>
      </>
    )
}