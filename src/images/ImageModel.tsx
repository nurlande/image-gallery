export interface Image {
    "albumId": number;
    "id": number;
    "title": string;
    "url": string;
    "thumbnailUrl": string
}

export interface QueryParams {
    page: number;
    sort: "asc" | "desc" | "none";
    filterAid: number | "All"; 
}

