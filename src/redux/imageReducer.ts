import { Image } from "../images/ImageModel";
import { DELETE_IMAGE, FETCHING_IMAGES, GET_IMAGES, UPDATE_IMAGES } from "./types";


export interface ImageState {
  images: any[];
  allImages: Image[];
  albums: any[];
  loading: boolean;
}

const initialState: ImageState = {
  images: [],
  allImages: [],
  albums: [],
  loading: false
};

export default function imageReducer(state = initialState, action: any) {
    switch (action.type) {
        case FETCHING_IMAGES:
            return {
                ...state,
                loading: true
            }
        case GET_IMAGES:
            return {
                ...state,
                images: action.payload,
                albums: action.albums,
                allImages: action.allData,
                loading: false
            }
        case UPDATE_IMAGES:
            return {
                ...state,
                images: action.payload
            }
        case DELETE_IMAGE:
            return {
                ...state,
                images: action.payload,
                allImages: action.allData
            }
        default:
            return {
                ...state
            }
    }
}
