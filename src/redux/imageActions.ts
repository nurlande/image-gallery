import { AppThunk } from "./store";
import { DELETE_IMAGE, FETCHING_IMAGES, GET_IMAGES, UPDATE_IMAGES } from "./types";
import {Image, QueryParams} from "../images/ImageModel"

interface Group {
    "page": number;
    "data": Image[]
}


export const setImages = (queryParams: QueryParams): AppThunk => (
    dispatch, getState
) => {
    let allImgs = [...getState().image.allImages]
    // get all existing images on redux store

    if(queryParams.sort) {
        allImgs = sortImages(queryParams.sort === "desc", allImgs)
        // sort if sort parameters exist
    }
    if(queryParams.filterAid && queryParams.filterAid !== "All") {
        allImgs = filterImages(Number(queryParams.filterAid), allImgs)
        // filter if filter parameters exist
    }

    const {images} = groupImages(allImgs, 12)
    // group images into pages

    dispatch({type: UPDATE_IMAGES, payload: images})
}

export const fetchingImages = (): AppThunk => (
    dispatch, getState
) => {
    dispatch({type: FETCHING_IMAGES})
} // set fetching state while fetching images

export const getImages = (list: Image[]): AppThunk => (
    dispatch,
    getState
  ) => {
        const {albums, images} = groupImages(list, 12)
        
        dispatch({type: GET_IMAGES, payload: images, allData: list, albums: albums});
}
// get all images data and set into redux store


export const deleteImage = (id: number | any, page: number): AppThunk => (
    dispatch, getState 
) => {
    let allImgs = [...getState().image.allImages]
    allImgs = allImgs.filter((img: Image) => img.id !== Number(id))
    // delete defined image from all list

    let groupedImgs = [...getState().image.images]

    let pageData = {...groupedImgs.find(
                            (im: Group) => im.page === Number(page)
                        )
                    }
    pageData.data = [...pageData.data].filter((img: Image) => img.id !== id)
    // delete defined image from defined page

    dispatch({
        type: DELETE_IMAGE, 
        payload: [...groupedImgs.filter((im: Group) => im.page !== Number(page)), pageData], 
        allData: [...allImgs]})
}


const groupImages = (images: Image[], imagesPerPage: number = 20) => {
    let groupData: Image[] = []
    let page = 1

    let albumIds: number[] = []

    let imgs: Group[] = []
    images.forEach((img: Image, index) => {
    
        !albumIds.includes(img.albumId) && albumIds.push(img.albumId)
        groupData.push(img)
    
        if ((index + 1) % imagesPerPage === 0){
            imgs.push({ page: page, data: [...groupData] })
            groupData.length = 0
            page++
        }
    })

    // grouping images into pages and extracting album ids from all list for filter option

    return {images: imgs, albums: albumIds}
}

export const filterImages = (albumId: number, imgs: Image[]) => {
    return imgs.filter((img: Image) => img.albumId === albumId)
} // function for filtering images by album id

export const sortImages = (descending: boolean, imgs: Image[]) => {
    const sortRule = (a: Image, b: Image) => {
        return !descending ? (a.albumId > b.albumId ? 1 : -1) : (a.albumId > b.albumId ? -1 : 1)
    }
    return imgs.sort((a: Image, b: Image) => sortRule(a, b))
} // function for sorting images by album id  ascending or descending. Default is ascending

